# Paweł Martenka  

e-mail: pawel.martenka@gmail.com  
mobile: +48 604 804 643  
Kamionki, near Poznań

# Personal summary
Professional senior software developer in Java and NodeJS, with 10+ years of experience in significant software projects. Currently, backend developer for any kind of services, eager to learn new technologies to meet customers' needs. As a project manager, I worked with recognizable customers on Polish logistics market. Oriented on clean, well-designed and maintainable software products, always ready to resolve project problems. Additionally, university teacher with a few years of experience in software related topics.

## Work experience

### 2019 - present, Espeo Software Sp. z o. o., senior software developer
Espeo Software is a software house providing web and mobile solutions on-demand.

### 2016 – 2019, Consdata Sp. z o. o., senior java developer
Consdata provides software solutions for banks, financial companies and the public sector. Eximee is a solution for internet forms, with high availability and asynchronous processing, usually tightly integrated with customer’s flow system. Frontend follows responsive web design rules. The customers of Eximee are the biggest Polish banks.

Technologies: Java, Spring/SpringBoot, Tomcat, Camel, ServiceMix, TypeScript, HTML/SCSS, AngularJS, SQL, JUnit/Mockito, Linux

Accomplishments:
 - Microservice driven solution with Angular frontend,  for data mapping between Eximee and customer flow system; software supporting internet form design process,
 - Eximee deployment for corporate and retail banking; direct support for customer’s system developers,
 - Microservices for custom form processing, supplementing Eximee main functionalities,
 - Vehicle insurance form design and development; high business value,
 - Trainings for customer employees - Eximee platform, Eximee architecture, business logic design and development, internet form design,
 - Multiple smaller change requests, business-driven form and process changes,
 - Mentoring for interns and new employees, initial trainings.

### 2012 – present, Poznań University of Technology, university teacher
University teacher at postgraduate studies of Software Engineering, software testing (main topics and technologies: Java, Junit, EasyMock, TDD, test cases and test plans, based on ISTQB Foundation Level),

### 2008 – 2016, PSI Polska Sp. z o. o., developer
Technologies: Java, C++, Perl, Oracle, Windows and AIX

Accomplishments:
 - Development of server and desktop client of wms system,
 - Testing and deployment on local and customer servers,
 - On-site commissioning and take over,
 - Tweaking of performance capabilities of wms server,
 - Creation of custom wms extensions and solutions, run by specific customer needs:
   - Tweaking of automated transport selection (internal warehouse logistics),
   - Algorithmic solutions for goods reservation (full box and sorted), automated and manual processes,
   - Automatic conveyors, mini-load, printers and sorters integration,
   - Receipt and delivery of goods processes, with conveyor system.

### 2013 – 2016, PSI Polska Sp. z o. o., project manager
PSI Polska provides software solutions for industry. Involved in development of warehouse management and material flow control systems, as developer, project and quality manager.

Accomplishments:
 - Project manager and a team leader with service for customers, managing a few projects from requirements, specification and implementation to testing, commissioning and take over.
 - Main customer is a leading Polish clothing company, with one of the biggest distribution centers in Poland.
 - Ran warehouse management system deployment for this customer from contract to project take over; the warehouse is state of the art fully automated distribution center.
 - Managing a small team (five members) of consultants and developers, to provide service with SLA for a five customers (warehouse management systems and material flow control systems); intense knowledge management, especially for long-term projects and support, English and Polish communication.

Started as Java and C++ developer for warehouse management system for a one customer, finished as a project manager and a team leader with service for customers, managing a few projects from requirements, specification and implementation to testing, commissioning and take over. Main customer is a leading Polish clothing company, with one of the biggest distribution centers in Poland. Ran warehouse management system deployment for this customer from contract to project take over; the warehouse is state of the art fully automated distribution center.

### 2016 – 2019, Collegium Da Vinci, university teacher
University teacher at postgraduate studies of Teaching computer science and computer classes, algorithms and data structures,

### 2011 – 2012, WSB Schools of Banking, Poznań, university teacher
Main duties:
- University teacher at undergraduate part-time studies, basics of programming (main topics and technologies: .NET/C#, data structures, algorithms, object-oriented design),
- Creation of syllabus.

## Skills
- `■ ■ ■ ■ ■` Java, Spring, Apache Tomcat
- `■ ■ ■ ■ □` NodeJS, TypeScript, ExpressJS
- `■ ■ ■ ■ ■` JUnit, Mockito, AssertJ
- `■ ■ ■ □ □` Hibernate, MyBatis, SQL
- `■ ■ ■ ■ □` ci/cd, git, mvn, Linux, MacOs
- `■ ■ □ □ □` Docker, Ansible, shell, Python
- `■ ■ ■ □ □` AngularJS/ReactJS, HTML, SCSS, yarn/npm/nvm
- `■ ■ ■ ■ □` Scrum, JIRA
- `■ ■ ■ ■ ■` Design patterns, clean code, OOP
- `■ ■ ■ ■ ■` IntelliJ, vi, VS Code

## Education
- 2009 – 2013 Poznań University of Technology, Faculty of Computing Science, Institute of Computing Science, part-time doctoral studies,
- 2008 – 2009 Poznań University of Technology, Faculty of Computing Science and Management, Institute of Computing Science, graduate full-time studies in computer science graduated with title of Master of Science; specialty: Software Engineering,
- 2004 – 2008 Poznań University of Technology, Faculty of Computing Science and Management, Institute of Computing Science, undergraduate full-time studies in computer science graduated with title of Bachelor of Science,
- ISTQB foundation level.

## Interest
Literature, bass player, home cinema, tourism, diy, mtb, photography.

## Agreement
I hereby agree for my personal data, included in my job application, to be processed in line with the needs of recruitment, in accordance with the Law on Personal Data Protection of 29 August 1997 (Law Gazette from 2002, No.101, heading 926, as amended).
